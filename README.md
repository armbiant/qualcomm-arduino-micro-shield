# ACJ Shield for Arduino Micro
![3D-CAD image of ACJ shield](PCB/ACJ_MicroShield_3d.PNG)
## Description
This project defines a Printed Circuit Board that replaces the original one in a Bacho ACJ air-heating system.  
The EDA tool that has been used for this is the [NI Circuit Design Suite](https://www.ni.com/sv-se/shop/electronic-test-instrumentation/application-software-for-electronic-test-and-instrumentation-category/what-is-multisim.html).  

The software needed can be found in the corresponding [firmware project](https://gitlab.com/acj-ctrl/sw/arduino-micro-shield-fw)

## License
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

## Project status
This hardware project isn't active right now, since the current design have been working fine for years now, and I don't yet have any needs to enhance it further.
